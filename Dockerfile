##########################################################################
### Builder
##########################################################################
FROM alpine:3.15.0 as builder
ARG VERSION=v0.33.2

WORKDIR /opt

RUN apk add --no-cache curl=7.80.0-r0 && \
    curl --fail --silent -L -o tflint.zip "https://github.com/terraform-linters/tflint/releases/download/${VERSION}/tflint_linux_amd64.zip" && \
    unzip tflint.zip && \
    rm -rf /var/cache/apk/*

##########################################################################
### tflint image
##########################################################################
FROM alpine:3.15.0

COPY --from=builder ./opt/tflint /usr/local/bin/tflint
